package com.norith.elasticemail

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import grails.util.Holders
import spock.lang.Specification

/**
 * Created by stephen on 2014-09-25.
 */
@TestMixin(GrailsUnitTestMixin)
class ElasticEmailEmailBuilderSpec extends Specification {
	private static final String USERNAME = ''
	private static final String API_KEY = ''

	private static final String RECIPIENT = 'recipient@example.org'
	private static final String RECIPIENT2 = 'recipient@example.com'
	private static final String SENDER = 'sender@example.org'
	private static final String MESSAGE_TEXT = 'This is a test'
	private static final String HTML_TEXT = '<h1>Hello World!</h1>'
	private static final String SUBJECT = 'Hello there'

	private static final Map<String, String> DEFAULT_CONFIG = [
			'username': USERNAME,
			'api_key': API_KEY
	]

	ElasticEmailEmailBuilder createBuilder() {
		return new ElasticEmailEmailBuilder().from(SENDER).addTo(RECIPIENT).addTo(RECIPIENT2).subject(SUBJECT).body(MESSAGE_TEXT)
	}

	def setup() {
		Holders.grailsApplication.config.grails.plugin.elasticemail = DEFAULT_CONFIG
	}

	def cleanup() {
	}

	void "Builder can build emails"() {
		given:
			ElasticEmailEmail email = createBuilder().build()

		expect:
			email.from == SENDER
			email.to == [RECIPIENT, RECIPIENT2]
			email.subject == SUBJECT
			email.body == MESSAGE_TEXT

		when:
			email = createBuilder().html(HTML_TEXT).build()

		then:
			email.from == SENDER
			email.to == [RECIPIENT, RECIPIENT2]
			email.subject == SUBJECT
			email.html == HTML_TEXT
	}
}
