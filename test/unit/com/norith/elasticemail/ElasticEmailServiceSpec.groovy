package com.norith.elasticemail

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ElasticMailService)
class ElasticEmailServiceSpec extends Specification {
	private static final String USERNAME = ''
	private static final String API_KEY = ''

	private static final String RECIPIENT = 'recipient@example.org'
	private static final String RECIPIENT2 = 'recipient@example.com'
	private static final String SENDER = 'sender@example.org'
	private static final String MESSAGE_TEXT = 'This is a test'
	private static final String SUBJECT = 'Hello there'

	private static final Map<String, String> DEFAULT_CONFIG = [
	        'username': USERNAME,
			'api_key': API_KEY
	]

	ElasticEmailEmailBuilder createBuilder() {
		return new ElasticEmailEmailBuilder().from(SENDER).addTo(RECIPIENT).addTo(RECIPIENT2).subject(SUBJECT).body(MESSAGE_TEXT)
	}

    def setup() {
	    grailsApplication.config.grails.plugin.elasticemail = DEFAULT_CONFIG
    }

    def cleanup() {
	}

    void "Send an email"() {
	    given:
		    ElasticEmailEmail email = createBuilder().build()
		    service.elasticMailConnectorService = Mock(ElasticMailConnectorService)

	    when:
		    service.send(email)

	    then:
		    1 * service.elasticMailConnectorService.send(email)
		    0 * _
    }

    void 'Send an email using the mail plugin DSL'() {
	    given:
	        service.elasticMailConnectorService = Mock(ElasticMailConnectorService)

	    when:
	        service.sendMail {
		        to this.RECIPIENT
		        from this.SENDER
		        subject this.SUBJECT
		        body this.MESSAGE_TEXT
	        }

	    then:
	        1 * service.elasticMailConnectorService.send({ ElasticEmailEmail email ->
			        email.to == this.RECIPIENT
			        email.from == this.SENDER
			        email.subject == this.SUBJECT
			        email.body == this.MESSAGE_TEXT
	            } as ElasticEmailEmail)
	        0 * _
    }

}
