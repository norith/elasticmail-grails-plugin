package com.norith.elasticemail

import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.test.mixin.TestFor
import org.springframework.http.ResponseEntity
import spock.lang.Specification


/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ElasticMailConnectorService)
class ElasticEmailConnectorServiceSpec extends Specification {
	static final String ELASTICMAIL_RESULT_TRANSACTION_ID = 'f74b9f96-f89a-4cfe-813f-5f86df1cb37f'

	private static final String USERNAME = 'test'
	private static final String API_KEY = 'test_key'

	private static final String RECIPIENT = 'recipient@example.org'
	private static final String RECIPIENT2 = 'recipient@example.com'
	private static final String SENDER = 'sender@example.org'
	private static final String MESSAGE_TEXT = 'This is a test'
	private static final String SUBJECT = 'Hello there'

	private static RestBuilder mockRestBuilder
	private static RestResponse mockRestReponse
	private static ResponseEntity mockRestResponseEntity

	private static final Map<String, String> DEFAULT_CONFIG = [
			'username': USERNAME,
			'api_key': API_KEY
	]

	ElasticEmailEmailBuilder createBuilder() {
		return new ElasticEmailEmailBuilder().from(SENDER).addTo(RECIPIENT).addTo(RECIPIENT2).subject(SUBJECT).body(MESSAGE_TEXT)
	}

	def setup() {
		grailsApplication.config.grails.plugin.elasticemail = DEFAULT_CONFIG

		mockRestBuilder = Mock(RestBuilder)
		mockRestReponse = Mock(RestResponse, constructorArgs: [Mock(ResponseEntity)])
		mockRestResponseEntity = Mock(ResponseEntity)

		service.metaClass.getRestBuilder = {
			return mockRestBuilder
		}
	}

	def cleanup() {
	}

	void "connector provides username and api key to api"() {
		when:
			def result = service.send(new ElasticEmailEmail())

		then:
			1 * mockRestBuilder.post(ElasticMailConnectorService.ELASTICMAIL_URL, _) >> mockRestReponse
			1 * mockRestReponse.getStatus() >> 200
			1 * mockRestReponse.hasBody() >> true
			1 * mockRestReponse.getResponseEntity() >> mockRestResponseEntity
			1 * mockRestResponseEntity.getBody() >> ELASTICMAIL_RESULT_TRANSACTION_ID
			0 * _

		and:
			result.success
			result.transactionID == ELASTICMAIL_RESULT_TRANSACTION_ID
	}

	void "connector understands send error notice"() {
		when:
			def result = service.send(new ElasticEmailEmail())

		then: 'ElasticMail returns an error'
			1 * mockRestBuilder.post(ElasticMailConnectorService.ELASTICMAIL_URL, _) >> mockRestReponse
			1 * mockRestReponse.getStatus() >> 200
			1 * mockRestReponse.hasBody() >> true
			1 * mockRestReponse.getResponseEntity() >> mockRestResponseEntity
			1 * mockRestResponseEntity.getBody() >> 'Error: unknown error'
			0 * _

		and: 'service fails'
			!result.success
	}

}
