package com.norith.elasticemail

import grails.test.spock.IntegrationSpec
import grails.util.Holders
import spock.lang.Ignore

/**
 * Created by stephen on 2014-09-29.
 */
class ElasticEmailServiceIntegrationSpec extends IntegrationSpec {

	private static final String USERNAME = '<your_email>'
	private static final String API_KEY = '<your_api_key>'
	private static final String EMAIL_ADDRESS = '<your_email@domain.com>'

	private static final Map<String, String> DEFAULT_CONFIG = [
			'username': USERNAME,
			'api_key': API_KEY
	]

	def elasticMailService

	def setup() {
		Holders.grailsApplication.config.grails.plugin.elasticemail = DEFAULT_CONFIG
	}

	def cleanup() {
	}

	/**
	 * Ignore this test unless you fill out your USERNAME, API_KEY and email address
	 */
	@Ignore
	def 'send an email'() {
		given:
			ElasticEmailSendResponse result = elasticMailService.sendMail {
				to EMAIL_ADDRESS
				from EMAIL_ADDRESS
				subject 'email from integration test'
				body 'content'
			} as ElasticEmailSendResponse

		expect:
			result.success
	}
}
