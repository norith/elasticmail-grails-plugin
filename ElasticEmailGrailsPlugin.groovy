class ElasticEmailGrailsPlugin {
	// the plugin version
	def version = "0.1"
	// the version or versions of Grails the plugin is designed for
	def grailsVersion = "2.4 > *"
	// resources that are excluded from plugin packaging
	def pluginExcludes = [
			"grails-app/views/error.gsp"
	]

	def loadAfter = ["mail"]

	def title = "Grails ElasticEmail Plugin" // Headline display name of the plugin

	def author = "Stephen Smith"
	def authorEmail = "stephen+elasticemail@norith.com"
	def description = '''\
Plugin to send email via ElasticEmail API.

Thanks to Antony Jones for the Sendgrid Grails plugin as inspiration

## Installation:
In your BuildConfig.groovy file:

	....
	plugins {
		...
		compile ':elasticemail:0.1\'
		...
	}

In your config.groovy file:

	grails {
		plugin {
			elasticemail {
				username = 'thecraphammer@gmail.com'
				api_key = 'b75d01b2-f5c1-4c2f-b12b-f3882172b041'
			}
		}
	}

'''

	// URL to the plugin's documentation
	def documentation = "https://bitbucket.org/norith/elasticmail-grails-plugin"

	// Extra (optional) plugin metadata

	// License: one of 'APACHE', 'GPL2', 'GPL3'
   def license = "APACHE"

	// Details of company behind the plugin (if there is one)
//    def organization = [ name: "My Company", url: "http://www.my-company.com/" ]

	// Any additional developers beyond the author specified above.
//    def developers = [ [ name: "Joe Bloggs", email: "joe@bloggs.net" ]]

	// Location of the plugin's issue tracker.
    def issueManagement = [ system: "BitBucket", url: "https://bitbucket.org/norith/elasticmail-grails-plugin/issues" ]

	// Online location of the plugin's browseable source code.
    def scm = [ url: "https://bitbucket.org/norith/elasticmail-grails-plugin/src" ]

	def doWithWebDescriptor = { xml ->
		// TODO Implement additions to web.xml (optional), this event occurs before
	}

	def doWithSpring = {
		springConfig.addAlias('mailService', 'elasticMailService')
	}

	def doWithDynamicMethods = { ctx ->
		// TODO Implement registering dynamic methods to classes (optional)
	}

	def doWithApplicationContext = { ctx ->
		// TODO Implement post initialization spring config (optional)
	}

	def onChange = { event ->
		// TODO Implement code that is executed when any artefact that this plugin is
		// watching is modified and reloaded. The event contains: event.source,
		// event.application, event.manager, event.ctx, and event.plugin.
	}

	def onConfigChange = { event ->
		// TODO Implement code that is executed when the project configuration changes.
		// The event is the same as for 'onChange'.
	}

	def onShutdown = { event ->
		// TODO Implement code that is executed when the application shuts down (optional)
	}
}
