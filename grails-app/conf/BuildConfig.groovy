grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"

grails.project.fork = [
    // configure settings for compilation JVM, note that if you alter the Groovy version forked compilation is required
    //  compile: [maxMemory: 256, minMemory: 64, debug: false, maxPerm: 256, daemon:true],

    // configure settings for the test-app JVM, uses the daemon by default
    test: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, daemon:true],
    // configure settings for the run-app JVM
    run: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
    // configure settings for the run-war JVM
    war: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
    // configure settings for the Console UI JVM
    console: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256],

    // disable forked execution so that the IDE can connect directly to the VM rather than remotely
    test: false,
    run: false
]

grails.project.work.dir = "target"
grails.project.dependency.resolver = "maven" // or maven
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {

	    grailsPlugins()
	    grailsHome()
        grailsCentral()

        mavenLocal()
        mavenCentral()
        // uncomment the below to enable remote dependency resolution
        // from public Maven repositories
        //mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
        //mavenRepo "http://repository.jboss.com/maven2/"
    }

	def gebVersion = "0.9.2"

    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.
        // runtime 'mysql:mysql-connector-java:5.1.27'

	    compile 'commons-codec:commons-codec:1.9'

	    test "cglib:cglib:3.1", {
		    export = false
	    }
    }

    plugins {
	    compile ":rest-client-builder:2.0.4-SNAPSHOT"

	    // http://grails-plugins.github.io/grails-release/docs/manual/
	    build(':release:3.0.1') {
		    export = false
	    }

	    // more easily set version of app
	    // Usage:  grails version-update x.x.+ //OR// use (M)ajor, (m)inor or (p)atch: grails version-update p -- to increment by one M.m.p
	    // http://grails.org/plugin/version-update
	    provided ":version-update:1.3.2"
    }
}
