package com.norith.elasticemail

import com.norith.elasticemail.exceptions.EmailSendCommunicationsException
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.codehaus.groovy.grails.exceptions.GrailsConfigurationException
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap

import java.util.regex.Pattern

class ElasticMailConnectorService {
	static final String ELASTICMAIL_URL = 'https://api.elasticemail.com/mailer/send'

	static final Pattern SUCCESS_MATCH_PATTERN = ~/^[0-9|a-f]{8}-[0-9|a-f]{4}-[0-9|a-f]{4}-[0-9|a-f]{4}-[0-9|a-f]{12}$/
	static final Pattern ERROR_MATCH_PATTERN = ~/^Error:/
	static final Pattern UNAUTHORIZED_MATCH_PATTERN = ~/^Unauthorized/

	static transactional = false

	GrailsApplication grailsApplication

	/**
	 * Send an email via ElasticMail
	 *
	 * Called by ElasticMailService
	 *
	 * @param email
	 * @return null if failed send or transaction ID String code which can be queried separately for statistics
	 * @throws GrailsConfigurationException
	 * @throws EmailSendCommunicationsException
	 */
	public ElasticEmailSendResponse send(ElasticEmailEmail email) throws GrailsConfigurationException, EmailSendCommunicationsException {
		ConfigObject config = this.getMailConfig()

		String username = config?.username
		String api_key = config?.api_key

		if (!username || !api_key) {
			throw new GrailsConfigurationException('no ElasticMail config found for elasticmail.username and/or elasticmail.api_key')
		}

		Map<String, String> credentials = ['username' : username, 'api_key' : api_key]
		MultiValueMap<String, String> formParameters = new LinkedMultiValueMap<String, String>()    // NOTE: form body values must be multivaluemap to work

		RestBuilder rb = getRestBuilder();
		RestResponse resp

		try {
			// Multivalue maps expect that the 'value' be a linked list so add them using the API
			credentials.each() { key, value ->
				formParameters.set(key, value)
			}
			email.toMap().each() { key, value ->
				formParameters.set(key, value)
			}

			resp = rb.post(ELASTICMAIL_URL) {
				contentType "application/x-www-form-urlencoded"
				body(formParameters)
			}
		}
		catch (RuntimeException re) {
			log.error('ElasticMail send resulted in ', re)
			throw new EmailSendCommunicationsException('RuntimeException from ElasticEmail Send', re)
		}
		catch (Exception e) {
			log.error('ElasticMail send resulted in ', e)
			throw new EmailSendCommunicationsException('Exception from ElasticEmail Send', e)
		}

		ElasticEmailSendResponse emsr = new ElasticEmailSendResponse()
		emsr.success = false
		emsr.statusCode = resp?.getStatus()

		if (emsr.statusCode != 200) {
			emsr.failureNotice = 'HTTP response code != 200 [' + emsr.statusCode + ']'
			return emsr
		}

		if (!resp?.hasBody()) {
			emsr.failureNotice = 'No response body'
			return emsr
		}

		String returnCode
		try {
			returnCode = resp.getResponseEntity().getBody()
		}
		catch (Exception e) {
			System.out.println(e.getMessage())
		}
		catch (RuntimeException re) {
			System.out.println(re.getMessage())
		}

		if (returnCode ==~ UNAUTHORIZED_MATCH_PATTERN || returnCode ==~ ERROR_MATCH_PATTERN || !(returnCode ==~ SUCCESS_MATCH_PATTERN)) {
			emsr.failureNotice = returnCode
			return emsr
		}

		emsr.success = true
		emsr.transactionID = returnCode

		return emsr
	}

	public getRestBuilder() {
		new RestBuilder()
	}

	private ConfigObject getMailConfig() {
		grailsApplication.config.grails?.plugin?.elasticemail
	}
}
