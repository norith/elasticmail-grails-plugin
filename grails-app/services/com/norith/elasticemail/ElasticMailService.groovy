package com.norith.elasticemail

import com.norith.elasticemail.exceptions.EmailSendCommunicationsException
import org.codehaus.groovy.grails.exceptions.GrailsConfigurationException


class ElasticMailService {

	static transactional = false

	ElasticMailConnectorService elasticMailConnectorService

	/**
	 * Public service to send an email via ElasticMail
	 *
	 * @param email
	 * @return null if failed send or transaction ID String code which can be queried separately for statistics
	 * @throws GrailsConfigurationException
	 * @throws EmailSendCommunicationsException
	 */
	public ElasticEmailSendResponse send(ElasticEmailEmail email) throws GrailsConfigurationException, EmailSendCommunicationsException {
		return elasticMailConnectorService.send(email)
	}

	/**
	 * GroovyMail compatible send email method
	 *
	 * @param closure
	 * @return null if failed send or transaction ID String code which can be queried separately for statistics
	 * @throws GrailsConfigurationException
	 * @throws EmailSendCommunicationsException
	 */
	public ElasticEmailSendResponse sendMail(Closure closure) throws GrailsConfigurationException, EmailSendCommunicationsException {
		ElasticEmailSendMailDSLDelegate delegate = new ElasticEmailSendMailDSLDelegate()

		closure.delegate = delegate
		closure.resolveStrategy = Closure.DELEGATE_ONLY
		closure.call()

		return send(delegate.build())
	}

}
