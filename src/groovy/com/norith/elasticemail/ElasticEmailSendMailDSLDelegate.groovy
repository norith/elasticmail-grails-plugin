package com.norith.elasticemail

/**
 * Allows GrailsMail plugin style DSL
 *
 * Created by stephen on 2014-09-25.
 */
class ElasticEmailSendMailDSLDelegate {

	ElasticEmailEmailBuilder builder = new ElasticEmailEmailBuilder()

	def methodMissing(String method, args) {
		return builder."${method}"(*args)
	}

	ElasticEmailEmailBuilder to(String... recipients) {
		recipients.each { String recipient ->
			builder.addTo(recipient)
		}

		return builder
	}

	ElasticEmailEmailBuilder body(String body) {
		return builder.body(body)
	}

	ElasticEmailEmailBuilder html(String html) {
		return builder.html(html)
	}
}
