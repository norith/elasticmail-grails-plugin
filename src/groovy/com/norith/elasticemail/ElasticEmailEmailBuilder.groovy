package com.norith.elasticemail

import com.norith.elasticemail.exceptions.InvalidElasticEmailException
import org.apache.commons.validator.GenericValidator

/**
 * Created by stephen on 2014-09-24.
 */
class ElasticEmailEmailBuilder {

	private ElasticEmailEmail email

	ElasticEmailEmailBuilder() {
		this.email = new ElasticEmailEmail()
	}

	/**
	 * Set the from email address
	 *
	 * @param sender
	 * @param senderName
	 * @return
	 * @throws IllegalArgumentException
	 */
	ElasticEmailEmailBuilder from(String sender, String senderName = null) throws IllegalArgumentException {
		if (senderName) {
			email.fromName = senderName
		}

		if (!GenericValidator.isEmail(sender)) {
			throw new IllegalArgumentException(('Invalid from email address = ' + sender) as String[])
		}

		email.from = sender

		return this
	}

	/**
	 * Add a to email address
	 *
	 * @param to
	 * @return
	 * @throws IllegalArgumentException
	 */
	ElasticEmailEmailBuilder addTo(String to) throws IllegalArgumentException {
		if (!GenericValidator.isEmail(to)) {
			throw new IllegalArgumentException(('Invalid to email address = ' + to) as String[])
		}

		email.setTo(to)

		return this
	}

	/**
	 * Set the subject
	 *
	 * @param subject
	 * @return
	 */
	ElasticEmailEmailBuilder subject(String subject) {
		email.subject = subject

		return this
	}

	/**
	 * Set the plain text body content
	 *
	 * @param body
	 * @return
	 * @throws IllegalArgumentException
	 */
	ElasticEmailEmailBuilder body(String body) throws IllegalArgumentException {
		if (body?.trim()) {
			body = body.trim()
		}
		else
			throw new IllegalArgumentException(('Empty body content = ' + body) as String[])

		email.body = body

		return this
	}

	/**
	 * Set the html body content
	 *
	 * @param html
	 * @return
	 * @throws IllegalArgumentException
	 */
	ElasticEmailEmailBuilder html(String html) throws IllegalArgumentException {
		if (html?.trim()) {
			html = html.trim()
		}
		else
			throw new IllegalArgumentException(('Empty html content = ' + html) as String[])

		email.html = html

		return this
	}

	/**
	 * Set the reply-to email address and optionally the reply-to name
	 *
	 * @param replyAddress
	 * @param replayName
	 * @return
	 * @throws IllegalArgumentException
	 */
	ElasticEmailEmailBuilder replyTo(String replyAddress, String replayName = null) throws IllegalArgumentException {
		if (replayName) {
			email.replyToName << replayName
		}

		if (!GenericValidator.isEmail(replyAddress)) {
			throw new IllegalArgumentException(('Invalid replyTo email address = ' + replyAddress) as String[])
		}

		email.replyTo = replyAddress

		return this
	}

	/**
	 * Construct an EE email object from the settings you've made on this builder
	 *
	 * @return
	 */
	ElasticEmailEmail build() {
		validateRequiredParameters()

		return this.email
	}

	/**
	 * Make sure that the 'to, subject and from' fields have been set and there is either a plain text and/or HTML body
	 *
	 * @throws InvalidElasticEmailException
	 */
	private validateRequiredParameters() throws InvalidElasticEmailException {
		['to', 'subject', 'from'].each { String field ->
			if (!this.email[field]) {
				throw new InvalidElasticEmailException([field])
			}
		}

		if (!(this.email.body || this.email.html)) {
			throw new InvalidElasticEmailException(['body', 'html'])
		}
	}

}
