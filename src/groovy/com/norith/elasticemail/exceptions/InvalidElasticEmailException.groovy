package com.norith.elasticemail.exceptions

/**
 * Created by stephen on 2014-09-24.
 */
class InvalidElasticEmailException extends RuntimeException {

	InvalidElasticEmailException(def properties) {
		super(String.format("Invalid ElasticMail email. Property %s has and invalid value, or was missing", properties))
	}
}
