package com.norith.elasticemail.exceptions

import groovy.transform.InheritConstructors

/**
 * Created by stephen on 2014-09-25.
 */
@InheritConstructors
class EmailSendCommunicationsException extends RuntimeException {
}
