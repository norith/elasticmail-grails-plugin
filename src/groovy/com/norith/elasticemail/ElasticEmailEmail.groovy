package com.norith.elasticemail

/**
 * An object representing the fields and body content of an email
 *
 * Do not create one yourself, use the EmailBuilder class
 *
 * ## Note:
 * ElasticEmail does not support 'to' email address names. The only names supported are the
 * 'fromName' and 'replyToName. ElasticEmail does not support Bcc or Cc addresses. 'To' addresses are treated
 * as 'Bcc' while still identifying the specific recipient a copy is being sent 'To'. Also, Group names are
 * not supported
 *
 * ## Status:
 * 20140930 -- v0.1: does not currently support file attachments or custom headers
 *
 * Created by stephen on 2014-09-24.
 */
class ElasticEmailEmail {

	List<String> to = []
	String from
	String fromName
	String subject
	String body
	String html
	String replyTo
	String replyToName

	private allParameters = [
			to: 'to',
			from: 'from',
			fromName: 'from_name',
			subject: 'subject',
			body: 'body_text',
			html: 'body_html',
			replyTo: 'reply_to',
			replyToName: 'reply_to_name',
			]

	Map<String, Object> toMap() {
		Map<String, Object> parameters = [:]

		parameters.putAll(encodeParameters())

		return parameters
	}

	private Map<String, Object> encodeParameters() {

		Map<String, Object> parameters = [:]

		allParameters.each { String internalName, String externalName ->
			Object value = this[internalName]
			if (value) {
				parameters.put(externalName, map(value))
			}
		}

		return parameters
	}

	private String map(List<String> values) {
		return values.join(';')
	}

	private String map(String string) {
		return string
	}

	public void setTo(String recipient) {
		this.to << recipient
	}
}
