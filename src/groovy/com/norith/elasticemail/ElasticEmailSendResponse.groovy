package com.norith.elasticemail

/**
 * The response object returned after sending an email via ElasticEmailService
 *
 * Success indicated by 'success' boolean with 'failureNotice' providing more info if success == false.
 * If successful, 'transactionID' can be used to track future events (opens, click-thrus etc...)
 *
 * ## Status
 * 20140930 -- v0.1: Tracking of future events via transactionID not yet supported
 *
 * Created by stephen on 2014-09-26.
 */
class ElasticEmailSendResponse {
	boolean success = false

	int statusCode = 0

	String failureNotice = null

	String transactionID = null


}
