# ElasticEmail Grails Plugin

## Summary
Plugin to send email via ElasticEmail API.

Thanks to Antony Jones for the Sendgrid Grails plugin as inspiration

## Installation
In your BuildConfig.groovy file:

	....
	plugins {
		...
		compile ':elasticemail:0.1'
		...
	}

In your Config.groovy file:

	grails {
		plugin {
			elasticemail {
				username = 'thecraphammer@gmail.com'
				api_key = 'b75d01b2-f5c1-4c2f-b12b-f3882172b041'
			}
		}
	}



## Example Usage
	// ... controller definition
	ElasticMailService elasticMailService               // spring Service injection

	def actionName() {
		// ...

		ElasticEmailEmailBuilder builder = new ElasticEmailEmailBuilder()
		builder.from('from@domain.com', 'First Last')
			.addTo('receipient@domain.com')
			.subject('Email Subject')
			.body('This content will be the body copy of the email')

		ElasticEmailSendResponse response = null
		try {
			response = elasticMailService.send(builder.build())
		}
		catch (InvalidElasticEmailException ieee) {
			...
		}
		catch (GrailsConfigurationException gce) {
			...
		}
		catch (EmailSendCommunicationsException esce) {
			...
		}

		if (response?.success) {
			// email sent
		}

		// ...
	}



## Notes:
	* ElasticEmail does not support 'to' email address names. The only names supported are the 'fromName' and 'replyToName.
	* ElasticEmail does not support Bcc or Cc addresses.
	* 'To' addresses are treated as 'Bcc' while still identifying the specific recipient a copy is being sent 'To'.
	* Also, Group names are not supported.

## Status:
### v0.1
	* 20140930
		* does not currently support file attachments or custom headers
		* tracking of future events (opens, click-thrus) via transactionID not yet supported
